<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Transaction
 * &@property Datatables $datatable
 */
class Transaction extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("Appmodel", "app_model");
		$this->load->model("Balancemodel", "balance_model");
		$this->load->library('form_validation');
	}

	function index()
	{
		if ($this->session->userdata('is_logged_in')) {
			$this->db->select('t.*,vm.via');
			$this->db->from('transaction_record t');
			$this->db->join('via_master vm','vm.id = t.via_id','left');
			$this->db->order_by('id','desc');
			$transaction_data = $this->db->get()->result();
			$via_data = $this->db->order_by('via','asc')->get('via_master')->result();
			$account_data = $this->db->select('account')->group_by('account')->get('transaction_record')->result();

			//for open 
			$open_summary = $this->app_model->open_summary();

			//for close
			$close_summary = $this->app_model->close_summary();

			//for gold booking near open , close and booking
			$booking_gold = $this->app_model->booking_gold_summary();

			if(!empty($booking_gold)){
				foreach($booking_gold as $k => $v){
					if(!empty($v['g1']) && !empty($v['gold_weight'])){
						$booking_gold[$k]['gold_average'] = $v['g1']/$v['gold_weight'];	
					}else{
						$booking_gold = null;	
					}
				}	
			}


			//for silver booking near open , close and booking
			$booking_silver = $this->app_model->booking_silver_summary();
			if(!empty($booking_silver)){
				foreach($booking_silver as $k => $v){
					if(!empty($v['s1']) && !empty($v['silver_weight'])){
						$booking_silver[$k]['silver_average'] = $v['s1']/$v['silver_weight'];
					}else{
						$booking_silver = null;	
					}
				}	
			}

			$account_array = $this->app_model->account_array();
			$bs_result = array();
			$inc = 0;
			foreach ($account_array as $k => $v) {
				$bs_g_result = array();
				$bs_s_result = array();
				$id = $v['account_id'];
				$bs_g_result = $this->app_model->bs_g_summary($id);
				$bs_s_result = $this->app_model->bs_s_summary($id);

				$bs_result[$inc]['account_id'] = $v['account_id']; 
				$bs_result[$inc]['account_name'] = $v['account_name']; 
				$bs_result[$inc]['gold_weight'] = $bs_g_result[0]['gold_weight']; 
				$gold_average = 0;
				if(!empty($bs_g_result[0]['gold_weight'])){
					$gold_average = $bs_g_result[0]['t1']/$bs_g_result[0]['gold_weight'];
				} else {
					$bs_g_result[0]['gold_weight'] = 0;
				}
				$bs_result[$inc]['gold_average'] = $gold_average; 
				$bs_result[$inc]['silver_weight'] = $bs_s_result[0]['silver_weight']; 
				$silver_average = 0;
				if(!empty($bs_s_result[0]['silver_weight'])){
					$silver_average = $bs_s_result[0]['t1']/$bs_s_result[0]['silver_weight'];
				} else {
					$bs_s_result[0]['silver_weight'] = 0;
				}
				$bs_result[$inc]['silver_average'] = $silver_average;

				$transaction_summary = $this->app_model->transaction_summary($id);
				$bs_result[$inc]['total_amount'] = $transaction_summary[0]['total_amount'];
				$bs_result[$inc]['total_gold'] = $transaction_summary[0]['total_gold'];
				$bs_result[$inc]['total_silver'] = $transaction_summary[0]['total_silver'];

				$inc++;
			}

			$this->load->view('transaction', array('transaction_data' => $transaction_data,'account_data' => $account_data,'via_data' => $via_data,'bs_result' => $bs_result, 'booking_gold' => $booking_gold, 'booking_silver' => $booking_silver,'open_summary' => $open_summary[0], 'close_summary' => $close_summary[0]));
		}else {
			redirect('/auth/login/');
		}
	}

	function get_allaccount() {
		$result = $this->app_model->get_allMasterRecored('account','account_id','account_name');
		$recored_arr = array();
		foreach($result as $recored_row){
			$recored_arr[] = $recored_row;
		}
		echo json_encode($recored_arr);
		return $recored_arr;
	}
	function get_allvia(){
		$result = $this->app_model->get_allMasterRecored('via_master','id','via');
		$recored_arr = array();
		foreach($result as $recored_row){
			$recored_arr[] = $recored_row;
		}
		echo json_encode($recored_arr);
		return $recored_arr;
	}

	function save()
	{
		$transaction_data = $this->input->post();
		
		$account = $transaction_data['account'];
		$account_id = 0;
		if(!empty($account)){
			if(is_numeric($account)){
				$account_id = $account;
			} else {
				$account = trim($account);
				$res = $this->app_model->get_result_where('account','account_name',$account);
				if(empty($res)){
					$account_id = $this->app_model->insert('account',array("account_name" => $account, "created_at" => date('Y-m-d H:i:s')));
				} else {
					$account_id = $res[0]->account_id;
				}
			}
		}

		$transaction_record_data['account'] = $account_id;
		$transaction_record_data['date'] = date('Y-m-d',strtotime($transaction_data['date']));
		$transaction_record_data['created_at'] = date('Y-m-d H:i:s');
		if($transaction_data['gold_type']!='')
		{
			$transaction_record_data['via_id'] = 7;
			$transaction_record_data['silver'] = 0;
			//gold+ and Rs - 
			if($transaction_data['gold_type']=='1_Bank_Axis')
			{
				$transaction_record_data['gold'] = $transaction_data['cut_fine'];
				$transaction_record_data['amount'] = ($transaction_data['cut_gold']*-1);
			}
			//gold- and Rs +
			else if($transaction_data['gold_type']=='1_Bank_HDFC'){
				$transaction_record_data['gold'] = ($transaction_data['cut_fine']*-1);
				$transaction_record_data['amount'] = $transaction_data['cut_gold'];
			}
			$transaction_record_data['notes']=$transaction_data['cut_note'];
		}

		$transaction_record_data['rs_effect_balance']=0;
		$transaction_record_data['gold_effect_balance']=0;
		$transaction_record_data['silver_effect_balance']=0;

		$this->db->insert('transaction_record', $transaction_record_data);
		$last_insert_id = $this->db->insert_id();
		if ($last_insert_id > 0) {
			$this->db->where('account_id',$account_id);
			$this->db->update('account',array('last_updated' => date('Y-m-d H:i:s')));
			
			$this->db->where('account_id',$account_id);
			$this->db->update('account',array('last_updated' => date('Y-m-d H:i:s')));

			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Transaction successfully added.');
			redirect(base_url() . 'transaction/');
		}
	}

	/**
	 * Get current pending for display popup on Transation booking screen
	 * if any Booking pending...
	 */
	function getCurrentPendingBooking() {

		$post = $this->input->post();
		// fields
		$fields = array('id','weight', 'pending_weight', 'rate', 'booking_date');

		// condtions
		$condtions = array();
		$condtions[] = array('column'=>'account = ', 'value' => $post['account_name'], 'condtion' => 'where');
		$condtions[] = array('column'=>'pending_weight > ', 'value' => 0, 'condtion' => 'where');

		// sort by
		$sort = array('booking_date'=>'asc');

		$getData = $this->app_model->getData('booking',$fields, $condtions, $sort);

		if (count($getData) > 0) {
			$results = [];

			$expected_utilised_weight = $post['trans_gold'];

			foreach ($getData as $key => $value) {
				$results[$key] = $value;

				$results[$key]->booking_date = date("d-m-Y", strtotime($value->booking_date));
				
				if ($expected_utilised_weight >= $value->pending_weight) {
					$results[$key]->taken = $value->pending_weight;
				} else if ($expected_utilised_weight < $value->pending_weight) {
					$results[$key]->taken = $expected_utilised_weight;
				}

				$expected_utilised_weight = $expected_utilised_weight - $results[$key]->taken;

			}
			echo json_encode (array('found' => true, 'results' => $results));
			exit;
		}
		echo json_encode (array('found' => false));
		exit;
	}

	function saveOpen()
	{
		$transaction_data = $this->input->post();
		//print_r($transaction_data);exit;
		$account = $transaction_data['account1'];
		$account_id = 0;
		if (!empty($account)) {
			if(is_numeric($account)){
				$account_id = $account;
			} else {
				$account = trim($account);
				$res = $this->app_model->get_result_where('account','account_name',$account);
				if(empty($res)){
					$account_id = $this->app_model->insert('account',array("account_name" => $account, "created_at" => date('Y-m-d H:i:s')));
				} else {
					$account_id = $res[0]->account_id;
				}
			}
		}
		$transaction_record_data['account'] = $account_id;
		$transaction_record_data['date'] = date('Y-m-d',strtotime($transaction_data['date1']));
		$transaction_record_data['created_at'] = date('Y-m-d H:i:s');

		//$transaction_record_data['via_id'] = $transaction_data['via_id1'];
		//$transaction_record_data['silver'] = 0;
		//gold+ and Rs - 

		$transaction_record_data['gold'] = $transaction_data['gold1'];
		$transaction_record_data['silver'] = $transaction_data['silver1'];
		$transaction_record_data['amount'] = $transaction_data['amount1'];
		//gold- and Rs +			

		$transaction_record_data['notes']=$transaction_data['tran_note1'];		

		$transaction_record_data['rs_effect_balance'] = 0;
		$transaction_record_data['gold_effect_balance'] = 0;
		$transaction_record_data['silver_effect_balance'] = 0;

		$this->db->insert('transaction_record', $transaction_record_data);
		$last_insert_id = $this->db->insert_id();
		if ($last_insert_id > 0) {
			$this->db->where('account_id',$account_id);
			$this->db->update('account',array('last_updated' => date('Y-m-d H:i:s')));

			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Open Transaction successfully added.');
			redirect(base_url() . 'transaction/');
		}
	}

	function saveFullTransaction()
	{
		$transaction_data = $this->input->post();
		$userdata = $this->session->userdata();
		
		
		$admin_id = $userdata['is_logged_in']['admin_id'];

		$account = $transaction_data['account'];

		$account_id = 0;

		if (!empty($account)) {

			if (is_numeric($account)) {
				$account_id = $account;
			} else {
				$account = trim($account);

				$res = $this->app_model->get_result_where('account','account_name',$account);

				if (empty($res)) {
					$account_id = $this->app_model->insert('account',array("account_name" => $account, "created_at" => date('Y-m-d H:i:s')));
				} else {
					$account_id = $res[0]->account_id;
				}
			}
		}

		$transaction_record_data['account'] = $account_id;

		$transaction_record_data['date'] = date('Y-m-d',strtotime($transaction_data['date']));

		$transaction_record_data['created_at'] = date('Y-m-d H:i:s');

		$via = $transaction_data['via_id'];

		if (!empty($via)) {
			if (is_numeric($via)) {
				$via_id = $via;
			} else {
				$via = trim($via);
				$res = $this->app_model->get_result_where('via_master','via',$via);
				if (empty($res)) {
					$$transaction_record_data['via_id'] = $this->app_model->insert('via_master',array("via" => $via, "updated_at" => date('Y-m-d H:i:s')));
				} else {
					$$transaction_record_data['via_id'] = $res[0]->id;
				}
			}
		}

		$transaction_record_data['gold'] = $transaction_data['gold'];
		$transaction_record_data['silver'] = $transaction_data['silver'];
		$transaction_record_data['amount'] = $transaction_data['amount'];
		//gold- and Rs +			

		$transaction_record_data['notes']=$transaction_data['tran_note'];		

		$transaction_record_data['rs_effect_balance'] = 1;
		$transaction_record_data['gold_effect_balance'] = 1;
		$transaction_record_data['silver_effect_balance'] = 1;

		$this->db->insert('transaction_record', $transaction_record_data);
		$last_insert_id = $this->db->insert_id();
		

		if ($last_insert_id > 0) {

			if (isset($transaction_data['taken']) && !empty ($transaction_data['taken'])) {

				foreach ($transaction_data['taken'] as $booking_id => $taken) {
					
					$trans_book_log_data = [];
					
					if ($taken > 0) {
						$trans_book_log_data['trans_id'] = $last_insert_id;
						$trans_book_log_data['book_id'] = $booking_id;
						$trans_book_log_data['used_qty'] = $taken;
						$trans_book_log_data['created_by'] = $admin_id;
						$trans_book_log_data['created_at'] = date('Y-m-d H:i:s');

						$this->db->insert('trans_book_log', $trans_book_log_data);
						
						$q = " UPDATE booking SET pending_weight = pending_weight - $taken WHERE id = $booking_id";

						$this->db->query($q);
					}
				}

			}

			$this->db->where('account_id',$account_id);
			$this->db->update('account',array('last_updated' => date('Y-m-d H:i:s')));

			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Full Transaction successfully added.');
			redirect(base_url() . 'transaction/');
		}
	}

	/**
     * Expected Interview DataTable
     */
	function transaction_datatable_v1()
	{
		$as_on_data = date('Y-m-d',strtotime(trim($_POST['as_on_data'])));
		$this->db->select('date,account,SUM(amount) as amount,gold,silver');
		$this->db->from('transaction_record');
		$this->db->where('date <= ',$as_on_data);
		$this->db->group_by('account');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$recordsTotal = $query->num_rows();
			$data = array();
			foreach ($query->result() as $transaction_row) {
				$row = array();
				$row[] = date('d-m-Y',strtotime($transaction_row->date));
				$row[] = $transaction_row->account;

				$row[] = $transaction_row->gold;
				$row[] = $transaction_row->amount;
				$row[] = $transaction_row->silver;
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->db->group_by('account')->get('transaction_record')->num_rows(),
				"recordsFiltered" => $recordsTotal,
				"data" => $data,
			);
			//output to json format
			echo json_encode($output);
		}
	}

	/**
     * Expected Interview DataTable
     */
	function transaction_datatable()
	{
		$config['select'] = 'am.account_id, am.account_name, SUM(t.amount) as total_amount, SUM(t.gold) as total_gold, SUM(t.silver) as total_silver';
		$config['table'] = 'transaction_record t';
		$config['column_order'] = array(null, 't.date', 'am.account_id', 'am.account_name', 't.gold', 't.silver', 't.amount');
		$config['column_search'] = array('am.account_name', 't.date', 't.account', 't.gold', 't.silver', 't.amount','vm.via');
		$config['joins'][] = array('join_table'=>'via_master vm','join_by'=>'vm.id = t.via_id','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'account am','join_by'=>'am.account_id = t.account','join_type'=>'left');
		$config['order'] = array('am.last_updated' => 'desc');
		$config['group_by'] = 't.account';

		if(isset($_POST['as_on_data'])){
			$as_on_data = date('Y-m-d',strtotime(trim($_POST['as_on_data'])));
			$config['wheres'][] = array('column_name'=>'date <= ','column_value'=> $as_on_data );
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		foreach ($list as $transaction_row) {
			$row = array();
			$row[] = "<a onClick='get_ledger(account_".$transaction_row->account_id.")' id='account_".$transaction_row->account_id."' data-account-id='".$transaction_row->account_id."' data-account-name='".$transaction_row->account_name."' style='cursor:pointer;' >".$transaction_row->account_name."</a>";
			$row[] = $transaction_row->total_amount;
			$row[] = $transaction_row->total_gold;
			$row[] = $transaction_row->total_silver;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	function ledger_datatable()
	{
		$config['select'] = 't.id, t.date, vm.via, t.amount, t.gold, t.silver, t.notes';
		$config['table'] = 'transaction_record t';
		$config['column_order'] = array(null, 't.date', 't.gold', 't.amount', 't.silver', 'vm.via', 't.notes');
		$config['column_search'] = array('t.date', 't.account', 't.gold', 't.silver', 't.amount','vm.via', 't.notes');
		$config['joins'][] = array('join_table'=>'via_master vm','join_by'=>'vm.id = t.via_id','join_type'=>'left');
		$config['joins'][] = array('join_table'=>'account am','join_by'=>'am.account_id = t.account','join_type'=>'left');
		$config['order'] = array('t.id' => 'desc');

		if (isset($_POST['as_on_data'])) {
			$as_on_data = date('Y-m-d',strtotime(trim($_POST['as_on_data'])));
			$config['wheres'][] = array('column_name'=>'date <= ','column_value'=> $as_on_data );
		}
		if (isset($_POST['selected_account_id'])) {
			$selected_account_id = $_POST['selected_account_id'];
			$config['wheres'][] = array('column_name'=>'t.account ','column_value'=> $selected_account_id );
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$total_amount = 0;
		$total_gold = 0;
		$total_silver = 0;
		foreach ($list as $transaction_row) {
			$row = array();
			$action = '';
			/*$action .= ' <a href="'.base_url('transaction/delete_ledger/'.$transaction_row->id).'" class="delete_button btn-danger btn-xs" data-href="'.base_url('transaction/delete_ledger/'.$transaction_row->id).'"><i class="fa fa-trash"></i></a>';*/
			$action .= ' <a href="javascript:void(0);" title="Delete this record" class="delete_button_ledger btn-danger btn-xs" data-href="'.base_url('transaction/delete/'.$transaction_row->id).'"><i class="fa fa-trash"></i></a>';
			$row[] = $action;
			$row[] = date('d-m-Y',strtotime($transaction_row->date));
			$total_amount += $transaction_row->amount;
			$row[] = $transaction_row->gold;
			$total_gold += $transaction_row->gold;
			$row[] = $transaction_row->amount;
			$row[] = $transaction_row->silver;
			$total_silver += $transaction_row->silver;
			$row[] = $transaction_row->via;
			$row[] = $transaction_row->notes;
			$data[] = $row;
		}
		$grant_total = array();
		$grant_total[] = '<b></b>';
		$grant_total[] = '<b>Total :</b>';
		if ($total_gold > 0) {
			$total_gold = '<b style="color:blue";>'.$total_gold.'</b>'; 
		} elseif ($total_gold < 0) {
			$total_gold = '<b style="color:red";>'.$total_gold.'</b>'; 
		}
		$grant_total[] = $total_gold;
		if ($total_amount > 0) {
			$total_amount = '<b style="color:blue";>'.$total_amount.'</b>'; 
		} elseif ($total_gold < 0) {
			$total_amount = '<b style="color:red";>'.$total_amount.'</b>'; 
		}
		$grant_total[] = $total_amount;
		if ($total_silver > 0){
			$total_silver = '<b style="color:blue";>'.$total_silver.'</b>'; 
		} elseif ($total_gold < 0){
			$total_silver = '<b style="color:red";>'.$total_silver.'</b>'; 
		}
		$grant_total[] = $total_silver;
		$grant_total[] = '';
		$grant_total[] = '';
		$data[] = $grant_total;

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	function delete_ledger($id)
	{
		$where_array['id'] = $id;
		$this->app_model->delete('transaction_record',$where_array);
		$this->session->set_flashdata('success',true);
		$this->session->set_flashdata('message','Deleted Successfully');
		redirect(base_url() . 'transaction/');
	}
	function booking_gold_datatable_detail()
	{
		$config['select'] = 'b.id, b.booking_date, b.rate, b.weight, b.mcx, b.booking_by';
		$config['table'] = 'booking b';
		$config['column_order'] = array(null, 'b.booking_date', 'b.rate', 'b.weight', 'b.mcx', 'b.rate', 'b.rate', 'b.booking_by');
		$config['column_search'] = array('b.booking_date', 'b.rate', 'b.weight', 'b.mcx');
		$config['joins'][] = array('join_table'=>'account am','join_by'=>'am.account_id = b.account','join_type'=>'left');
		$config['order'] = array('b.id' => 'desc');
		$config['wheres'][] = array('column_name'=>'type','column_value'=> 1 );

		if(isset($_POST['as_on_data'])){
			$as_on_data = date('Y-m-d',strtotime(trim($_POST['as_on_data'])));
			$config['wheres'][] = array('column_name'=>'booking_date <= ','column_value'=> $as_on_data );
		}
		if(isset($_POST['selected_account_id'])){
			$selected_account_id = $_POST['selected_account_id'];
			$config['wheres'][] = array('column_name'=>'b.account ','column_value'=> $selected_account_id );
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$total_amount = 0;
		$total_gold = 0;
		$total_silver = 0;
		foreach ($list as $booking_row) {
			$row = array();
			$action = '';
			$action .= '<a href="javascript:void(0);" title="Delete this record" class="delete_button_booking_gold btn-danger btn-xs" data-href="'.base_url('transaction/delete/'.$booking_row->id).'"><i class="fa fa-trash"></i></a>';
			$row[] = $action;
			$row[] = date('d-m-Y',strtotime($booking_row->booking_date));
			$row[] = number_format($booking_row->rate,3,'.','');
			$row[] = number_format($booking_row->weight,3,'.','');
			$row[] = number_format($booking_row->mcx,3,'.','');
			$row[] = number_format($booking_row->rate - $booking_row->mcx,3,'.','');
			$row[] = number_format($booking_row->rate * $booking_row->weight,3,'.','');
			if($booking_row->booking_by == 1){ $bc = 'Cash';}else{ $bc = 'Bill';}
			$row[] = $bc;
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	function booking_silver_datatable_detail()
	{
		$config['select'] = 'b.id, b.booking_date, b.rate, b.weight, b.mcx ,b.booking_by';
		$config['table'] = 'booking b';
		$config['column_order'] = array(null, 'b.booking_date', 'b.rate', 'b.weight', 'b.mcx', 'b.rate', 'b.rate' ,'b.booking_by');
		$config['column_search'] = array('b.booking_date', 'b.rate', 'b.weight', 'b.mcx');
		$config['joins'][] = array('join_table'=>'account am','join_by'=>'am.account_id = b.account','join_type'=>'left');
		$config['order'] = array('b.id' => 'desc');
		$config['wheres'][] = array('column_name'=>'type','column_value'=> 2 );

		if(isset($_POST['as_on_data'])){
			$as_on_data = date('Y-m-d',strtotime(trim($_POST['as_on_data'])));
			$config['wheres'][] = array('column_name'=>'booking_date <= ','column_value'=> $as_on_data );
		}
		if(isset($_POST['selected_account_id'])){
			$selected_account_id = $_POST['selected_account_id'];
			$config['wheres'][] = array('column_name'=>'b.account ','column_value'=> $selected_account_id );
		}

		$this->load->library('datatables', $config, 'datatable');
		$list = $this->datatable->get_datatables();
		$data = array();
		$total_amount = 0;
		$total_gold = 0;
		$total_silver = 0;
		foreach ($list as $booking_row) {
			$row = array();
			$action = '';
			$action .= '<a href="javascript:void(0);" title="Delete this record" class="delete_button_booking_silver btn-danger btn-xs" data-href="'.base_url('transaction/delete/'.$booking_row->id).'"><i class="fa fa-trash"></i></a>';
			//$action .= '<span class="" data-href="'.base_url('transaction/delete/'.$booking_row->id).'"></span>';
			$row[] = $action;
			$row[] = date('d-m-Y',strtotime($booking_row->booking_date));
			$row[] = number_format($booking_row->rate,3,'.','');
			$row[] = number_format($booking_row->weight,3,'.','');
			$row[] = number_format($booking_row->mcx,3,'.','');
			$row[] = number_format($booking_row->rate - $booking_row->mcx,3,'.','');
			$row[] = number_format($booking_row->rate * $booking_row->weight,3,'.','');
			if($booking_row->booking_by == 1){ $bc = 'Cash';}else{ $bc = 'Bill';}
			$row[] = $bc;
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->datatable->count_all(),
			"recordsFiltered" => $this->datatable->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	public function getOpenClose()
	{
		if(isset($_POST['as_on_data'])){
			$as_on_data = date('Y-m-d',strtotime(trim($_POST['as_on_data'])));
		}

		//fetch total for open
		$query =  $this->db->query("select IFNULL(SUM(gold),'') as gold, IFNULL(SUM(silver),'') as silver, IFNULL(SUM(amount),'') as amount from transaction_record where date < '".$as_on_data."' AND rs_effect_balance=1 AND gold_effect_balance=1 AND silver_effect_balance=1");
		if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				$open['gold']=$value['gold'];
				$open['silver']=$value['silver'];
				$open['amount']=$value['amount'];
			}
		}

		//fetch total for close
		$query =  $this->db->query("select IFNULL(SUM(gold),'') as gold, IFNULL(SUM(silver),'') as silver, IFNULL(SUM(amount),'') as amount from transaction_record where date <= '".$as_on_data."' AND rs_effect_balance=1 AND gold_effect_balance=1 AND silver_effect_balance=1");
		if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				$close['gold']=$value['gold'];
				$close['silver']=$value['silver'];
				$close['amount']=$value['amount'];
			}
		}

		$output['openData']=$open;
		$output['closeData']=$close;
		echo json_encode($output);
	}

	public function get_allaccount_direct()
	{
		if(isset($_POST['as_on_data'])){
			$as_on_data = date('Y-m-d',strtotime(trim($_POST['as_on_data'])));
			//$config['wheres'][] = array('column_name'=>'date <= ','column_value'=> $as_on_data );
		}
		//echo $as_on_data;
		$query =  $this->db->query("SELECT `transaction_record`.*, account.account_name, SUM(transaction_record.silver) as silver_total, SUM(transaction_record.gold) as gold_total, SUM(transaction_record.amount) as total_amount FROM transaction_record, account where transaction_record.account = account.account_id and date <= '".$as_on_data."' GROUP by transaction_record.account");
		//$data
		if($query->num_rows() > 0){
			foreach($query->result_array() as $key => $value)
			{
				$data.="<tr>";
				//print_r($value);
				$data.="<td>".$value['account_name']."</td>";
				$data.="<td>".$value['total_amount']."</td>";
				$data.="<td>".$value['gold_total']."</td>";
				$data.="<td>".$value['silver_total']."</td>";
				$data.="</tr>";
			}
		}
		echo $data;
	}

	function saveBook()
	{
		$book_data = $this->input->post();
		$account = $book_data['account3'];
		$account_id = 0;
		if(!empty($account)){
			if(is_numeric($account)){
				$account_id = $account;
			} else {
				$account = trim($account);
				$res = $this->app_model->get_result_where('account','account_name',$account);
				if(empty($res)){
					$account_id = $this->app_model->insert('account',array("account_name" => $account, "created_at" => date('Y-m-d H:i:s')));
				} else {
					$account_id = $res[0]->account_id;
				}
			}
		}

		$transaction_data['account_id'] = $account_id;
		$transaction_data['date'] = date('Y-m-d',strtotime($book_data['date3']));
		$transaction_data['rate'] = $book_data['amount3'];
		$transaction_data['weight'] = $book_data['weight3'];
		$transaction_data['MCX'] = 	$book_data['mcx3'];

		$this->db->insert('book', $transaction_data);
		if ($this->db->insert_id() > 0) {
			$this->db->where('account_id',$account_id);
			$this->db->update('account',array('last_updated' => date('Y-m-d H:i:s')));
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Book successfully added.');
			redirect(base_url() . 'transaction/');
		}
	}

	function balance()
	{
		$transaction=$this->balance_model->getAccountTransaction(10);
		$gold_bookings=$this->balance_model->getGoldBookings(10);
		echo "<pre>";
		print_r($transaction);
		print_r($gold_bookings);
		echo "</pre>";
		$this->load->view('balance');
	}
	
	function add_gold_booking()
	{
		$book_data = $this->input->post();
		$account = $book_data['booking_account_name'];
		$account_id = 0;
		if(!empty($account)){
			if(is_numeric($account)){
				$account_id = $account;
			} else {
				$account = trim($account);
				$res = $this->app_model->get_result_where('account','account_name',$account);
				if(empty($res)){
					$account_id = $this->app_model->insert('account',array("account_name" => $account, "created_at" => date('Y-m-d H:i:s')));
				} else {
					$account_id = $res[0]->account_id;
				}
			}
		}

		$booking_data['type'] = 1;
		$booking_data['booking_date'] = date('Y-m-d',strtotime($book_data['booking_date']));
		$booking_data['account'] = $account_id;
		$booking_data['rate'] = $book_data['booking_rate'];
		$booking_data['weight'] = $book_data['booking_weight'];
		$booking_data['mcx'] = 	$book_data['booking_mcx'];
		$booking_data['booking_by'] = 	$book_data['booking_by'];

		$this->db->insert('booking', $booking_data);
		if ($this->db->insert_id() > 0) {
			$this->db->where('account_id',$account_id);
			$this->db->update('account',array('last_updated' => date('Y-m-d H:i:s')));
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Booking added successfully.');
			redirect(base_url() . 'transaction/');
		}
	}
	function add_silver_booking()
	{
		$book_data = $this->input->post();
		$account = $book_data['booking_silver_account_name'];
		$account_id = 0;
		if(!empty($account)){
			if(is_numeric($account)){
				$account_id = $account;
			} else {
				$account = trim($account);
				$res = $this->app_model->get_result_where('account','account_name',$account);
				if(empty($res)){
					$account_id = $this->app_model->insert('account',array("account_name" => $account, "created_at" => date('Y-m-d H:i:s')));
				} else {
					$account_id = $res[0]->account_id;
				}
			}
		}

		$booking_data['type'] = 2;
		$booking_data['booking_date'] = date('Y-m-d',strtotime($book_data['booking_date']));
		$booking_data['account'] = $account_id;
		$booking_data['rate'] = $book_data['booking_rate'];
		$booking_data['weight'] = $book_data['booking_weight'];
		$booking_data['mcx'] = 	$book_data['booking_mcx'];
		$booking_data['booking_by'] = 	$book_data['booking_by'];

		$this->db->insert('booking', $booking_data);
		if ($this->db->insert_id() > 0) {
			$this->db->where('account_id',$account_id);
			$this->db->update('account',array('last_updated' => date('Y-m-d H:i:s')));
			$this->session->set_flashdata('success', true);
			$this->session->set_flashdata('message', 'Booking added successfully.');
			redirect(base_url() . 'transaction/');
		}
	}
	function delete($id)
	{
		$table = $_POST['table_name'];
		$id_name = $_POST['id_name'];
		if($table == 'account'){
			$this->app_model->delete('booking', array('account' => $id));	
			$this->app_model->delete('transaction_record', array('account' => $id));	
		}
		$this->app_model->delete($table, array($id_name => $id));
		$this->session->set_flashdata('success',true);
		$this->session->set_flashdata('message','Deleted Successfully');
		
		$this->session->set_flashdata('success', true);
		$this->session->set_flashdata('message', 'Full Transaction successfully added.');
			redirect(base_url() . 'transaction/');
		
		
		$return['success'] = "true";
		print json_encode($return);
		exit;
		//redirect(base_url() . 'transaction/');
	}
	function delete_account($id)
	{
		$table = 'account';
		$id_name = 'account_id';
		if($table == 'account'){
			$this->app_model->delete('booking', array('account' => $id));	
			$this->app_model->delete('transaction_record', array('account' => $id));	
		}
		$this->app_model->delete($table, array($id_name => $id));
		$this->session->set_flashdata('success',true);
		$this->session->set_flashdata('message',"User's Account And All Records Deleted Successfully");
		
		redirect(base_url() . 'transaction/');
		
	}
	
}
