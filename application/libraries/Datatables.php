<?php

/**
 * Class AppLib
 */
class Datatables
{
    var $select = '';
    var $table = '';
    var $joins = array();
    var $wheres = array();
    var $column_search = array();
    var $column_order = array();
    var $order = array();
    var $group_by = '';

    /**
     * Datatables constructor.
     * @param array $config
     */
    function __construct(array $config = array())
    {
        $this->ci =& get_instance();
        $this->ci->load->database();

        if (count($config) > 0)
        {
            $this->initialize($config);
        }
    }

    /**
     * Initialize preferences
     *
     * @param	array
     * @return	CI_Datatables
     */
    public function initialize($config = array())
    {
        foreach ($config as $key => $val)
        {
            if (isset($this->$key))
            {
                $this->$key = $val;
            }
        }
        return $this;
    }

    /**
     *
     */
    private function _get_datatables_query()
    {
        if($this->select != ''){
            $this->ci->db->select($this->select);
        }

        $this->ci->db->from($this->table);

        if(!empty($this->joins)){
            foreach($this->joins as $join){
                if(isset($join['join_table']) &&  isset($join['join_by']) && $join['join_table'] != '' && $join['join_by'] != ''){
                    $join_table = $join['join_table'];
                    $join_by = $join['join_by'];
                    $join_type = isset($join['join_type'])?$join['join_type']:'inner';
                    $this->ci->db->join("$join_table","$join_by","$join_type");
                }
            }
        }

        if(!empty($this->wheres)){
            foreach($this->wheres as $where){
                if(isset($where['column_name']) &&  isset($where['column_value']) && $where['column_name'] != '' && $where['column_value'] != ''){
                    $column_name = $where['column_name'];
                    $column_value = $where['column_value'];
                    $this->ci->db->where($column_name,$column_value);
                }
            }
        }


        $i = 0;
        foreach ($this->column_search as $item) // loop column
        {
            if(isset($_POST['search']['value'])) // if datatable send POST for search
            {
                if($i===0)
                {
                    $this->ci->db->group_start();
                    $this->ci->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->ci->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->ci->db->group_end();
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->ci->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->ci->db->order_by(key($order), $order[key($order)]);
        }

        if($this->group_by != '') // here order processing
        {
            $this->ci->db->group_by($this->group_by);
        }
    }

    /**
     * @return mixed
     */
    function get_datatables()
    {
        $this->_get_datatables_query();
        if(isset($_POST['length']) && $_POST['length'] != -1)
            $this->ci->db->limit($_POST['length'],$_POST['start']);
        $query = $this->ci->db->get();
        return $query->result();
    }

    /**
     * @return mixed
     */
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->ci->db->get();
        return $query->num_rows();
    }

    /**
     * @return mixed
     */
    public function count_all()
    {
        $this->ci->db->from($this->table);
        if(!empty($this->wheres)){
            foreach($this->wheres as $where){
                if(isset($where['column_name']) &&  isset($where['column_value']) && $where['column_name'] != '' && $where['column_value'] != ''){
                    $column_name = $where['column_name'];
                    $column_value = $where['column_value'];
                    $this->ci->db->where($column_name,$column_value);
                }
            }
        }
        return $this->ci->db->count_all_results();
    }
    
   
}
