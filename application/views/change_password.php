<?php if ($this->session->flashdata('success') == true) { ?>
    <script>
        $( document ).ready(function() {
            show_notify('<?php echo $this->session->flashdata('message'); ?>',true);    
        });
    </script>
<?php } ?>
<?php $this->load->library('form_validation');?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
    Change Password

    </h1>
    </section>

<!-- Main content -->
<section class="content">
    <div class="row">  <div class="col-md-12 display_alert"></div></div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <?=form_open(base_url('auth/change_password/') , array('id'=>'frm_cpwd','method'=>'post'));?>
                        <?=form_input(array('type' => 'hidden','name' => 'admin_id','value' => isset($admin_id)?$admin_id:'' ));?>
                        <div class="form-group">
                            <?= form_label('Old Password','',array('class'=>'control-labelsm'));?>
                            <?=form_input(array('type' => 'password','name' => 'old_pass','id' => 'pass','placeholder' => '', 'class' => 'form-control','autofocus'));?>

                            <?php if(isset($errors['old_pass'])){ 
                                    echo form_label($errors['old_pass'],'',array('id'=>'name-error','for'=>'old_pass','class'=>'control-label input-sm text-danger'));
                                } 
                            ?>

                        </div>
                        <div class="form-group">
                            <?= form_label('New Password','',array('class'=>'control-labelsm'));?>
                            <?=form_input(array('type' => 'password','name' => 'new_pass','id' => 'new_pass','placeholder' => '', 'class' => 'form-control','parsley-trigger' => 'change'));?>
                            <?php if(isset($errors['new_pass'])){
                                    echo form_label($errors['new_pass'],'',array('id'=>'name-error','for'=>'new_pass','class'=>'control-label input-sm text-danger'));
                                } 
                            ?>
                        </div>
                        <div class="form-group">
                            <?= form_label('Confirm Password','',array('class'=>'control-labelsm'));?>
                            <?=form_input(array('type' => 'password','name' => 'confirm_pass','id' => '','placeholder' => '', 'class' => 'form-control','parsley-trigger' => 'change'));?>
                            <?php if(isset($errors['confirm_pass'])){ 
                                    echo form_label($errors['confirm_pass'],'',array('id'=>'name-error','for'=>'confirm_pass','class'=>'control-label input-sm text-danger'));
                                } 
                            ?>
                        </div>
                        <div class="form-group text-right m-b-0">
                            <?= form_submit(array('id' => '', 'type' => 'submit', 'class' => 'btn btn-primary waves-effect waves-light', 'value' => 'Change')); ?>
                            <?= form_submit(array('id' => '', 'type' => 'reset', 'class' => 'btn  waves-effect ', 'value' => 'Reset')); ?>
                        </div>
                    <?=form_close();?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
<script>

  $("#frm_cpwd").validate({
    rules: {
      old_pass: "required",
      new_pass: "required",
      confirm_pass: {
        equalTo: "#new_pass",
        required: true
      }
    },
    messages: {
      old_pass: "Please provide a old password",
      new_pass: "Please provide a new password",
      confirm_pass: {
        required: "Please provide a conform password",
        equalTo: "Please enter the same password as above."
      }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });

</script>
        
