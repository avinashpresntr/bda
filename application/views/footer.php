<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>Version</b> 1.0
	</div>
	<strong>Copyright &copy; 2017 <a href="./">BDA</a>.</strong>
</footer>

<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->


<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/app.min.js');?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/plugins/sparkline/jquery.sparkline.min.js');?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
<script src="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/dist/js/demo.js');?>"></script>
<?php
if($this->uri->segment(1) == 'transaction'){
?>

<?php
}

?>

<!-- Select2 -->
<script src="<?= base_url()?>assets/plugins/select2/select2.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url()?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
	$(document).ready(function(){
		$('.select2').select2();
		$('.input-date-picker').datepicker({
			autoclose: true,
			format:'dd-mm-yyyy'
		});
	});
</script>






<script src="<?php echo base_url('assets/plugins/notify/jquery.growl.js');?>"></script>
<link rel="stylesheet" href="<?= base_url()?>assets/plugins/parsleyjs/src/parsley.css">
<script type="text/javascript" src="<?= base_url()?>assets/plugins/parsleyjs/dist/parsley.min.js"></script>
<script>
	$(document).ready(function () {
		$('form').parsley();
	});
	function show_notify(notify_msg,notify_type)
	{
		if(notify_type == true){
			$.growl.notice({ title:"Success!",message:notify_msg});
		}else{
			$.growl.error({ title:"False!",message:notify_msg});
		}
	}
</script>
<?php if($this->session->flashdata('success') === true  && $this->session->flashdata('message') != '') { ?>
<script>
	show_notify('<?= $this->session->flashdata('message') ?>',true);
</script>
<?php
																									   }
	  if($this->session->flashdata('success') === false  && $this->session->flashdata('message') != '') {
?>
<script>
	show_notify('<?= $this->session->flashdata('message') ?>',false);
</script>
<?php
																										}?>

</body>
</html>
