<?php
$this->load->view('header');
//$this->load->view('leftsidebar');
setlocale(LC_MONETARY, 'en_IN');
?>
<style>
	td.highlight_blue {
		color: blue;
	}
	td.highlight_red {
		color: red;
	}	
</style>
<div class="content-wrapper" style="font-size:18px">
	<div class="container-fluid">		
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<div class="clearfix"></div>
			<br>
			<h1>
				Transaction
				<small></small>
			</h1>			
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box col-md-12" style="padding:10px; ">
						<div class="form-group">							
							<div class="col-md-2">
								<!--<input type="text" class="form-control" placeholder="dd-mm-yyyy" name="as_on_data" id="as_on_data" value="<?=date('d-m-Y')?>">-->
								<label for="">Account:</label>
								<input type="hidden" name="account" id="account_name" autofocus style="width:100%;" required >
								<ul class="parsley-errors-list filled account_required"  style="display: none;">
									<li class="parsley-required">This value is required.</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- Custom Tabs -->
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab_booking_summary" data-toggle="tab" id="tab_booking_summary_tab">Summary</a></li>							
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_booking_summary">
								<div class="clearfix"></div>
								<div class="row">
									<div class="col-md-1"></div>
									<div class="col-md-10">
										<table class="table table-bordered text-right" id="table_booking_summary">
											<thead>
												<tr>
													<th class="text-right">Transaction</th>
													<th class="text-right" width="120px">Booking(weight)</th>
													<th class="text-right">Price</th>
													<th class="text-right">Grand price(Booking Weight * price)</th>
													<th class="text-right">Update Booking</th>
													<th class="text-right">Pending Gold weight</th>
													<th class="text-right">Debit Gold(0 if booking is there)</th>
													<th class="text-right">Pending silver weight</th>
													<th class="text-right">Debit silver(0 if booking is there)</th>
													
												</tr>
											</thead>
											<tbody>
											</tbody>
											<thead>
												<tr>
													<td><b></b></td>
													<td><b>Total</b></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>	
											</thead>
										</table>
									</div>
									<div class="col-md-1"></div>
								</div>
							</div>							
							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
					<!-- nav-tabs-custom -->
				</div>
				<!-- /.col -->
			</div>
		</section>
	</div>
</div>
<script>
	var table;
	$(document).ready(function(){
		
		account_name_load();
		
		
	function account_name_load(){
		$.ajax({
			url: '<?=base_url('transaction/get_allaccount');?>',
			dataType: 'json',
			success: function(result){
				$("#account_name,#cut_account_name,#account_name1").select2({
					createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
					multiple: true,
					maximumSelectionSize: 1,
					data: result,
				}).on('change', function (e) {

					//$("#account_name").val();
					console.log("on change:--"+$("#account_name").val());
					//this.value
				});
				$("#account_name3").select2({
					createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
					multiple: true,
					maximumSelectionSize: 1,
					data: result,
				});
				$("#account_name_silver3").select2({
					createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
					multiple: true,
					maximumSelectionSize: 1,
					data: result,
				});
				$("#booking_account_name").select2({
					createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
					multiple: true,
					maximumSelectionSize: 1,
					data: result,
				});
				$("#booking_silver_account_name").select2({
					createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
					multiple: true,
					maximumSelectionSize: 1,
					data: result,
				});
				function log(e) {
					var e=$("<li>"+e+"</li>");
					$("#events_11").append(e);
					e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
				}
				$("#account_name,#cut_account_name")
					.on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
					.on("select2-opening", function() { log("opening"); })
					.on("select2-open", function() { log("open"); })
					.on("select2-close", function() { log("close"); })
					.on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
					.on("select2-focus", function(e) { log ("focus");})
					.on("select2-blur", function(e) { log ("blur");});

			}
		});
	}
});
	
</script>
<?php
$this->load->view('footer');
?>

