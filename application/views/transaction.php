<?php
$this->load->view('header');
//$this->load->view('leftsidebar');
setlocale(LC_MONETARY, 'en_IN');
function check_numb($Degree){
	if ( $Degree > 0 ) {
		//echo 'style="color:red;';
		return 'style="color:blue";';
	} elseif( $Degree < 0 ) {
		//echo 'Negative';
		return 'style="color:red";';
	}
}
?>

<style>
	td.highlight_blue {
		color: blue;
	}
	td.highlight_red {
		color: red;
	}
</style>
<div class="content-wrapper" style="font-size:18px">
	<div class="container-fluid">
		<!--<div class="wrapper row-offcanvas row-offcanvas-left">
<div class="right-side strech">-->
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<div class="clearfix"></div>
			<br>
			<h1>
				Transaction
				<small></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Transaction</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="box">
				<div class="box-header">
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-3">
								<label>Open</label>
								<table class="table table-bordered text-right">
									<thead>
									</thead>
									<tbody id="open_table">
										<tr>
											<td>Gold</td>
											<td><?=$open_summary['open_gold']?></td>
										</tr>
										<tr>
											<td>Amount</td>
											<td><?=$open_summary['open_amount']?></td>
										</tr>
										<tr>
											<td>Silver</td>
											<td><?=$open_summary['open_silver']?></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-3">
								<label>Close</label>
								<table class="table table-bordered text-right">
									<thead>
									</thead>
									<tbody id="close_table">
										<tr>
											<td>Gold</td>
											<td><?=$close_summary['close_gold']?></td>
										</tr>
										<tr>
											<td>Amount</td>
											<td><?=$close_summary['close_amount']?></td>
										</tr>
										<tr>
											<td>Silver</td>
											<td><?=$close_summary['close_silver']?></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-3">
								<label>Booking</label>
								<table class="table table-bordered text-right">
									<thead>
									</thead>
									<tbody id="close_table">
										<tr>
											<td></td>
											<td>Weight</td>
											<td>Average</td>
										</tr>
										<tr>
											<td>Gold</td>
											<td><span <?= check_numb($booking_gold[0]['gold_weight']);?>><?=$booking_gold[0]['gold_weight']?></span></td>
											<td><span <?= check_numb($booking_gold[0]['gold_average']);?>><?=$booking_gold[0]['gold_average']?></span></td>
										</tr>
										<tr>
											<td>Silver</td>
											<td><span <?= check_numb($booking_silver[0]['silver_weight']);?>><?=$booking_silver[0]['silver_weight']?></span></td>
											<td><span <?= check_numb($booking_silver[0]['silver_average']);?>><?=$booking_silver[0]['silver_average']?></span></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer clearfix">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- Custom Tabs -->
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li><a href="#tab_transaction" data-toggle="tab" id="tab_transaction_tab">Open</a></li>
							<li class="active"><a href="#tab_full_transaction" data-toggle="tab" id="tab_full_transaction_tab">Transaction</a></li>
							<li><a href="#tab_gold_book" data-toggle="tab" id="tab_gold_book_tab">Gold Book</a></li>
							<li><a href="#tab_silver_book" data-toggle="tab" id="tab_silver_book_tab">Silver Book</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane bg-warning" id="tab_transaction" style="padding:1px 5px;">
								<form action="<?=base_url()?>transaction/saveOpen" method="post" data-parsley-validate novalidate >
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Date:</label>
												<input type="text" class="form-control input-date-picker" placeholder="dd-mm-yyyy" name="date1" value="<?=date('d-m-Y')?>" required>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Account:</label>
												<input type="hidden" name="account1" id="account_name1" autofocus style="width:100%;" required >
												<ul class="parsley-errors-list filled account_required"  style="display: none;">
													<li class="parsley-required">This value is required.</li>
												</ul>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Gold:</label>
												<input type="text" class="form-control" id="" placeholder="" name="gold1">
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="">RS:</label>
												<input type="text" class="form-control" id="" placeholder="" name="amount1">
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Silver:</label>
												<input type="text" class="form-control" id="" placeholder="" name="silver1">
											</div>
										</div>
										<div class="col-md-1">
											<div class="form-group">
												<label for="notes">Notes</label>
												<textarea name="tran_note1" class="form-control"></textarea>
											</div>
										</div>
										<div class="col-md-1">
											<div class="form-group">
												<label for="">&nbsp;</label><br/>
												<button type="submit" class="btn btn-primary submit_btn_open">Save</button>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane active bg-info" id="tab_full_transaction" style="padding:1px 10px;">
								<form action="<?=base_url()?>transaction/saveFullTransaction" method="post" data-parsley-validate novalidate >
									<div class="row">
										<div class="own-col">
											<div class="form-group">
												<label for="">Date:</label>
												<input type="text" class="form-control input-date-picker" placeholder="dd-mm-yyyy" name="date" value="<?=date('d-m-Y')?>" required>
											</div>
										</div>
										<div class="own-col">
											<div class="form-group">
												<label for="">Account:</label>
												<input type="hidden" name="account" id="account_name" autofocus style="width:100%;" required >
												<ul class="parsley-errors-list filled account_required"  style="display: none;">
													<li class="parsley-required">This value is required.</li>
												</ul>
											</div>
										</div>
										<div class="own-col">
											<div class="form-group">
												<label for="">Gold:</label>
												<input type="text" class="form-control" id="trans-gold" placeholder="" name="gold">
											</div>
										</div>
										<div class="own-col">
											<div class="form-group">
												<label for="">RS:</label>
												<input type="text" class="form-control" id="" placeholder="" name="amount">
											</div>
										</div>
										<div class="own-col">
											<div class="form-group">
												<label for="">Silver:</label>
												<input type="text" class="form-control" id="" placeholder="" name="silver">
											</div>
										</div>
										<div class="own-col">
											<div class="form-group">
												<label for="">Via:</label>
												<input type="hidden" name="via_id" id="ddl_via" style="width:100%;"/>
											</div>
										</div>
										<div class="own-col">
											<div class="form-group">
												<label for="notes">Notes</label>
												<textarea name="tran_note" class="form-control"></textarea>
											</div>
										</div>
										<div class="col-md-1">
											<div class="form-group">
												<label for="">&nbsp;</label><br/>
												<button type="submit" class="btn btn-primary submit_btn">Save</button>
											</div>
										</div>
									</div>
								
									<!-- Model :Start -->
									<!-- Modal -->
									<div id="myModal" class="modal fade" role="dialog">
										<div class="modal-dialog">

											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Pending Bookings</h4>
												</div>
												<div class="modal-body">
													<p>Pending booking will apear here...</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
												</div>
											</div>

										</div>
									</div>
									<!-- Model :End -->
								</form>
							</div>
							<div class="tab-pane bg-danger" id="tab_gold_book" style="padding:1px 5px 10px;">
								<form action="<?=base_url()?>transaction/add_gold_booking" method="post" data-parsley-validate novalidate >
									<input type="hidden" name="booking_type" id="booking_type" value="1">
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Date:</label>
												<input type="text" class="form-control input-date-picker" placeholder="dd-mm-yyyy" name="booking_date" value="<?=date('d-m-Y')?>" required>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Account:</label>
												<input type="hidden" name="booking_account_name" id="booking_account_name" style="width:100%;" required >
												<ul class="parsley-errors-list filled account_required"  style="display: none;">
													<li class="parsley-required">This value is required.</li>
												</ul>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Rate:</label>
												<input type="text" class="form-control" id="" placeholder="" name="booking_rate">
											</div>
										</div>
										<div class="col-md-3">
											<div class="col-md-6">
												<div class="form-group">
													<label for="">Weight:</label>
													<input type="text" class="form-control" id="" placeholder="" name="booking_weight">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="">MCX:</label>
													<input type="text" class="form-control" id="" placeholder="" name="booking_mcx">
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Booking:</label>
												<select class="select2" name="booking_by" id="select-via" style="width: 100%;">
													<option value="1">Cash</option>
													<option value="2" selected>Bill</option>
												</select>
											</div>
										</div>
										<div class="col-md-1">
											<div class="form-group">
												<label for="">&nbsp;</label><br/>
												<button type="submit" class="btn btn-primary booking_submit_btn">Save</button>
											</div>
										</div>
									</div>
								
								</form>
							</div>
							<div class="tab-pane" id="tab_silver_book" style="padding:1px 5px;">
								<form action="<?=base_url()?>transaction/add_silver_booking" method="post" data-parsley-validate novalidate >
									<input type="hidden" name="booking_type" id="booking_type" value="2">
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Date:</label>
												<input type="text" class="form-control input-date-picker" placeholder="dd-mm-yyyy" name="booking_date" value="<?=date('d-m-Y')?>" required>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Account:</label>
												<input type="hidden" name="booking_silver_account_name" id="booking_silver_account_name" style="width:100%;" required >
												<ul class="parsley-errors-list filled account_required"  style="display: none;">
													<li class="parsley-required">This value is required.</li>
												</ul>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Rate:</label>
												<input type="text" class="form-control" id="" placeholder="" name="booking_rate">
											</div>
										</div>
										<div class="col-md-3">
											<div class="col-md-6">
												<div class="form-group">
													<label for="">Weight:</label>
													<input type="text" class="form-control" id="" placeholder="" name="booking_weight">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="">MCX:</label>
													<input type="text" class="form-control" id="" placeholder="" name="booking_mcx">
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="">Booking:</label>
												<select class="select2" name="booking_by" id="select-via" style="width: 100%;">
													<option value="1">Cash</option>
													<option value="2" selected>Bill</option>
												</select>
											</div>
										</div>
										<div class="col-md-1">
											<div class="form-group">
												<label for="">&nbsp;</label><br/>
												<button type="submit" class="btn btn-primary booking_submit_btn">Save</button>
											</div>
										</div>
									</div>
								</form>
							</div>
							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
					<!-- nav-tabs-custom -->
				</div>
				<!-- /.col -->
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="box col-md-12" style="padding:10px; ">
						<div class="form-group">
							<label for="as_on_data" class="col-md-2" style="line-height: 30px;">As on date</label>
							<div class="col-md-2">
								<input type="text" class="form-control" placeholder="dd-mm-yyyy" name="as_on_data" id="as_on_data" value="<?=date('d-m-Y')?>">
							</div>
						</div>
					</div>
					<!-- Custom Tabs -->
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab_booking_summary" data-toggle="tab" id="tab_booking_summary_tab">Summary</a></li>
							<li class="tab_booking_detail_tab_class"><a href="#tab_booking_detail" data-toggle="tab" id="tab_booking_detail_tab" class="">Booking Detail</a></li>
							<li class="tab_ledger_tab_class"><a href="#tab_ledger" data-toggle="tab" id="tab_ledger_tab" class="">Ledger</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_booking_summary">
								<div class="clearfix"></div>
								<div class="row">
									<div class="col-md-1"></div>
									<div class="col-md-10">
										<table class="table table-bordered text-right" id="table_booking_summary">
											<thead>
												<tr>
													<th class="text-right">Action</th>
													<th class="text-right" width="120px">Account</th>
													<th class="text-right">Gold Weight</th>
													<th class="text-right">Gold Average</th>
													<th class="text-right">Gold</th>
													<th class="text-right">Rs</th>
													<th class="text-right">Silver</th>
													<th class="text-right">Silver Weight</th>
													<th class="text-right">Silver Average</th>

												</tr>
											</thead>
											<tbody>
												<?php
												$total_gold_weight = 0;
												$total_gold_average = 0;
												$total_silver_weight = 0;
												$total_silver_average = 0;
												$total_total_gold = 0;
												$total_total_amount = 0;
												$total_total_silver = 0;
												foreach($bs_result as $k => $v){
												?>
												<tr>
													<td nowrap>
														<a href="<?=base_url('transaction/delete_account/'.$v['account_id'])?>" title="Delete this record" class="btn-danger btn-xs" data-href="<?=base_url('transaction/delete/'.$v['account_id'])?>"><i class="fa fa-trash"></i></a>
														<button class="btn btn-xs btn-primary" onClick="get_booking_detail(booking_account_<?=$v['account_id']?>)" data-account-id = "<?=$v['account_id']?>" data-account-name = "<?=$v['account_name']?>" id="booking_account_<?=$v['account_id']?>">Booking Detail</button>
														<button class="btn btn-xs btn-primary" onClick='get_ledger(account_<?=$v['account_id']?>)' id='account_<?=$v['account_id']?>' data-account-id='<?=$v['account_id']?>' data-account-name='<?=$v['account_name']?>'>Ledger</button>
													</td>
													<td><?=$v['account_name']?></td>
													<td><span <?= check_numb($v['gold_weight']);?>><?=$v['gold_weight']?></span></td>
													<td><span <?= check_numb($v['gold_average']);?>><?=$v['gold_average']?></span></td>
													<td><span <?= check_numb($v['total_gold']);?>><?=$v['total_gold']?></span></td>
													<td><span <?= check_numb($v['total_amount']);?>><?=$v['total_amount']?></span></td>
													<td><span <?= check_numb($v['total_silver']);?>><?=$v['total_silver']?></span></td>
													<td><span <?= check_numb($v['silver_weight']);?>><?=$v['silver_weight']?></span></td>
													<td><span <?= check_numb($v['silver_average']);?>><?=$v['silver_average']?></span></td>
													<?php
													$total_gold_weight += $v['gold_weight'];
													$total_gold_average += $v['gold_average'];
													$total_total_gold += $v['total_gold'];
													$total_total_amount += $v['total_amount'];
													$total_total_silver += $v['total_silver'];
													$total_silver_weight += $v['silver_weight'];
													$total_silver_average += $v['silver_average'];
													?>
												</tr>
												<?php
												}
												?>
											</tbody>
											<thead>
												<tr>
													<td><b></b></td>
													<td><b>Total</b></td>
													<td><b><span <?= check_numb($total_gold_weight);?>><?=$total_gold_weight?></span></b></td>
													<td><b></b></td>
													<td><b><span <?= check_numb($total_total_gold);?>><?=$total_total_gold;?></span></b></td>
													<td><b><span <?= check_numb($total_total_amount);?>><?=$total_total_amount?></span></b></td>
													<td><b><span <?= check_numb($total_total_silver);?>><?=$total_total_silver?></span></b></td>
													<td><b><span <?= check_numb($total_silver_weight);?>><?=$total_silver_weight?></span></b></td>
													<td><b></b></td>

												</tr>	
											</thead>
										</table>
									</div>
									<div class="col-md-1"></div>
								</div>
							</div>
							<div class="tab-pane" id="tab_booking_detail">
								<div class="clearfix"></div>
								<div class="row">
									<div class="col-md-12 text-center">
										<input type="hidden" name="selected_booking_account_id" id="selected_booking_account_id" />
										<input type="text" name="selected_booking_account_name" id="selected_booking_account_name" value="" readonly class="text-center" style="width:250px; margin-bottom:10px; font-weight: bold;" />
									</div>
								</div>
								<div class="row">
									<div class="">

										<div class="col-md-6">
											<div class="table-responsive">
												<table class="table table-bordered text-right" id="table_booking_gold_detail">
													<thead>
														<tr>
															<th>Action</th>
															<th class="text-right">Date</th>
															<th class="text-right">Rate</th>
															<th class="text-right">Gold Weight</th>
															<th class="text-right">Mcx</th>
															<th class="text-right">Diff</th>
															<th class="text-right">Amount</th>
															<th class="">Booking By</th>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>
										</div>

									</div>


									<div class="">

										<div class="col-md-6">
											<div class="table-responsive">
												<table class="table table-bordered text-right" id="table_booking_silver_detail">
													<thead>
														<tr>
															<th>Action</th>
															<th class="text-right">Date</th>
															<th class="text-right">Rate</th>
															<th class="text-right">Silver Weight</th>
															<th class="text-right">Mcx</th>
															<th class="text-right">Diff</th>
															<th class="text-right">Amount</th>
															<th class="">Booking By</th>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>
										</div>

									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab_ledger">
								<div class="clearfix"></div>
								<div class="row">
									<div class="col-md-12 text-center">
										<input type="hidden" name="selected_account_id" id="selected_account_id" />
										<input type="text" name="selected_account_name" id="selected_account_name" value="" readonly class="text-center" style="width:250px; margin-bottom:10px; font-weight: bold;" />
									</div>
								</div>
								<table class="table table-bordered text-right" id="table_ledger">
									<thead>
										<tr>
											<th>Action </th>
											<th class="text-right" width="120px">Transaction Date </th>
											<th class="text-right">Gold</th>
											<th class="text-right">RS</th>
											<th class="text-right">Silver</th>
											<th class="text-right">Via</th>
											<th class="text-right">Notes</th>
										</tr>
									</thead>
									<tbody id = "ledger_data">
									</tbody>
								</table>
							</div>
							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
					<!-- nav-tabs-custom -->
				</div>
				<!-- /.col -->
			</div>
		</section>
		<!-- /.content -->
	</div>
</div>
<script>
	var table;
	
	/**
	 * Loading a popup for display if booking pending for current account
	 *
	 **/
	function loadBookingPopUp (results) {
		var $html = '<table class="table table-bordered">';
		$html += '	<thead>';
		$html += '		<tr>';
		$html += '			<th>ID</th>';
		$html += '			<th>Weight</th>';
		$html += '			<th>Pending Weight</th>';
		$html += '			<th>Rate</th>';
		$html += '			<th>Booking Date</th>';
		$html += '			<th>Taken Weight</th>';
		$html += '		</tr>';
		$html += '	</thead>';
		$(results).each(function (k,v) {
			$html += '	<tr>';
			$html += '		<td>';
			$html += v.id;
			$html += '		</td>';
			$html += '		<td>';
			$html += v.weight;
			$html += '		</td>';
			$html += '		<td>';
			$html += v.pending_weight;
			$html += '		</td>';
			$html += '		<td>';
			$html += v.rate;
			$html += '		</td>';
			$html += '		<td>';
			$html += v.booking_date;
			$html += '		</td>';
			$html += '		<td>';
			$html += '			<input type="text" name="taken['+v.id+']" value="'+v.taken+'" />';
			$html += '		</td>';
			$html += '	</tr>';
		});
		$html += '</table>';
		$(".modal-body").html($html);
		$("#myModal").modal("show");
	}

	$(document).ready(function(){
		// check if account-name or gold amount changed
		$('#account_name, #trans-gold').on('change', function (event) {
			// Define values intop variables
			var account_name = $('#account_name').val();
			var trans_gold = $('#trans-gold').val();
			
			/**
			 * Check if account name select value and gold input value
			 * is not empty then make ajax call of any booking pending
			 * for this account or not. If any account stands then a pop
			 * will be opened.
			 */
			
			if (account_name != '' && trans_gold != '' ) {
				// Ajax call
				data = {'account_name':account_name, 'trans_gold': trans_gold};
				$.ajax({
					url: '<?php echo base_url('transaction/getCurrentPendingBooking'); ?>',
					type: "POST",
					data: data,
					dataType: 'json',
					success: function(data) {
						if (data.found == true) {
							loadBookingPopUp(data.results);
						}
					},
					error: function(r) {
						alert ('Something went wrong!');
					}
				});
			}
		});
		$(document).on('keyup keypress keydown', ".select2-input", function (event) {
			if (event.which == 13) {
				if($('#account_name').val() != ''){
					$('input[name="gold"]').focus();
				}
				if($('#account1').val() != ''){
					$('input[name="gold1"]').focus();
				}
				if($('#booking_account_name').val() != ''){
					$('input[name="booking_rate"]').focus();
				}
				if($('#booking_silver_account_name').val() != ''){
					$('input[name="booking_rate"]').focus();
				}
			}
		});
		account_name_load();
		via_load();
		get_opneclose();
		$('.account_required').hide();
		$(document).on('click', '.submit_btn', function () {
			var account_name = $('#account_name').val();
			if(account_name == ''){
				$('.account_required').show();
				return false;
			}
		});
		$('.cut_account_required').hide();
		$(document).on('click', '.cut_submit_btn', function () {
			var account_name = $('#cut_account_name').val();
			if(account_name == ''){
				$('.cut_account_required').show();
				return false;
			}
		});
		$("#text_account_name").focus(); 
		$("input,select").bind("keydown",function(event) {
			if (event.which === 13) {
				event.stopPropagation();
				event.preventDefault();
				$(':input:eq(' + ($(':input').index(this) + 1) +')').focus();
			}
		});
		$(document).on('click', '#tab_transaction_tab', function () {
			$('#tab_summary_tab').click();
		});
		$(document).on('click', '#tab_full_transaction_tab', function () {
			$('#tab_summary_tab').click();
		});
		$(document).on('click', '#tab_gold_book_tab', function () {
			//$('#tab_booking_summary_tab').click();
		});
		$(document).on('click', '#tab_silver_book_tab', function () {
			//$('#tab_booking_summary_tab').click();
		});

		table = $('#table_transaction').DataTable({
			"processing": true,
			"serverSide": true,
			//"searching": false,
			"paging": false,
			"ordering": false,
			"info":     false,
			"order": [],
			"ajax": {
				"url": "<?php echo site_url('transaction/transaction-datatable')?>",
				"type": "POST",
				"data": function(d){
					d.as_on_data = $("#as_on_data").val();
				},
			},
			"columnDefs": [
				{
					"targets": [0],
					"orderable": false,
				},
			],
		});

		$('#table_booking_summary').DataTable({
			//"searching": false,
			"paging": false,
			//"ordering": false,
			"info":     false,
			"columnDefs": [
				{
					"targets": [0],
					"orderable": false,
				},
			],
		});
		$('#table_booking_silver_summary').DataTable({
			"searching": false,
			"paging": false,
			//"ordering": false,
			"info":     false,
			"columnDefs": [
				{
					"targets": [0],
					"orderable": false,
				},
			],
		});

		var as_on_data = $('#as_on_data').datepicker({
			autoclose: true,
			format:'dd-mm-yyyy'
		});



		ledger_datatable = $('#table_ledger').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"paging": false,
			//"ordering": false,
			"info":     false,
			"order": [],
			"ajax": {
				"url": "<?php echo site_url('transaction/ledger-datatable')?>",
				"type": "POST",
				"data": function(d){
					d.as_on_data = $("#as_on_data").val();
					d.selected_account_id = $("#selected_account_id").val();
				},
			},
			"columnDefs": [
				{
					"targets": [0],
					"orderable": false,
				},
			],
			"createdRow": function ( row, data, index ) {
				if ( data[2] > 0 ) {
					$('td', row).eq(2).addClass('highlight_blue');
				}else if(data[2] < 0){
					$('td', row).eq(2).addClass('highlight_red');
				}
				if ( data[3] > 0 ) {
					$('td', row).eq(3).addClass('highlight_blue');
				}else if(data[3] < 0){
					$('td', row).eq(3).addClass('highlight_red');
				}
				if ( data[4] > 0 ) {
					$('td', row).eq(4).addClass('highlight_blue');
				}else if(data[4] < 0){
					$('td', row).eq(4).addClass('highlight_red');
				}
			}
		});

		table_booking_gold_detail = $('#table_booking_gold_detail').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"paging": false,
			//"ordering": false,
			"info":     false,
			"order": [],
			"ajax": {
				"url": "<?php echo site_url('transaction/booking-gold-datatable-detail')?>",
				"type": "POST",
				"data": function(d){
					d.as_on_data = $("#as_on_data_booking").val();
					d.selected_account_id = $("#selected_booking_account_id").val();
				},
			},
			"columnDefs": [
				{
					"targets": [0],
					"orderable": false,
				},
			],
			"createdRow": function ( row, data, index ) {
				if ( data[2] > 0 ) {
					$('td', row).eq(2).addClass('highlight_blue');
				}else if(data[2] < 0){
					$('td', row).eq(2).addClass('highlight_red');
				}
				if ( data[3] > 0 ) {
					$('td', row).eq(3).addClass('highlight_blue');
				}else if(data[3] < 0){
					$('td', row).eq(3).addClass('highlight_red');
				}
				if ( data[4] > 0 ) {
					$('td', row).eq(4).addClass('highlight_blue');
				}else if(data[4] < 0){
					$('td', row).eq(4).addClass('highlight_red');
				}
				if ( data[5] > 0 ) {
					$('td', row).eq(5).addClass('highlight_blue');
				}else if(data[5] < 0){
					$('td', row).eq(5).addClass('highlight_red');
				}
				if ( data[6] > 0 ) {
					$('td', row).eq(6).addClass('highlight_blue');
				}else if(data[6] < 0){
					$('td', row).eq(6).addClass('highlight_red');
				}
			}
		});

		table_booking_silver_detail = $('#table_booking_silver_detail').DataTable({
			"processing": true,
			"serverSide": true,
			"searching": false,
			"paging": false,
			//"ordering": false,
			"info":     false,
			"order": [],
			"ajax": {
				"url": "<?php echo site_url('transaction/booking-silver-datatable-detail')?>",
				"type": "POST",
				"data": function(d){
					d.as_on_data = $("#as_on_data_booking").val();
					d.selected_account_id = $("#selected_booking_account_id").val();
				},
			},
			"columnDefs": [
				{
					"targets": [0],
					"orderable": false,
				},
			],
			"createdRow": function ( row, data, index ) {
				if ( data[2] > 0 ) {
					$('td', row).eq(2).addClass('highlight_blue');
				}else if(data[2] < 0){
					$('td', row).eq(2).addClass('highlight_red');
				}
				if ( data[3] > 0 ) {
					$('td', row).eq(3).addClass('highlight_blue');
				}else if(data[3] < 0){
					$('td', row).eq(3).addClass('highlight_red');
				}
				if ( data[4] > 0 ) {
					$('td', row).eq(4).addClass('highlight_blue');
				}else if(data[4] < 0){
					$('td', row).eq(4).addClass('highlight_red');
				}
				if ( data[5] > 0 ) {
					$('td', row).eq(5).addClass('highlight_blue');
				}else if(data[5] < 0){
					$('td', row).eq(5).addClass('highlight_red');
				}
				if ( data[6] > 0 ) {
					$('td', row).eq(6).addClass('highlight_blue');
				}else if(data[6] < 0){
					$('td', row).eq(6).addClass('highlight_red');
				}
			}
		});

		$(document).on('change','#as_on_data',function(){
			table.draw();
			ledger_datatable.draw();
			table_booking_gold_detail.draw();
			table_booking_silver_detail.draw();
			get_opneclose();

		});

		$(document).on("click",".delete_button_ledger",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=transaction_record',
					success: function(data){
						tr.remove();
						ledger_datatable.draw();
						show_notify("Delete Sucessfully.",true);
					}
				});
			}
		});

		$(document).on("click",".tab_booking_detail_tab_class",function(){
			//$('#selected_booking_account_id').val("");
			//$('#selected_booking_account_name').val("");
			table_booking_gold_detail.draw();
			table_booking_silver_detail.draw();
		});

		$(document).on("click",".tab_ledger_tab_class",function(){
			//alert($('#selected_booking_account_id').val());
			//$('#selected_booking_account_id').val("");
			ledger_datatable.draw();
		});

		$(document).on("click","#tab_booking_summary_tab",function(){
			table.draw();
			window.location.reload();
		});

		$(document).on("click",".delete_button_booking_gold",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=booking',
					success: function(data){
						tr.remove();
						table.draw();
						table_booking_gold_detail.draw();
						show_notify("Delete Sucessfully.",true);
					}
				});
			}
		});
		$(document).on("click",".delete_button_booking_silver",function(){
			var value = confirm('Are you sure delete this records?');
			var tr = $(this).closest("tr");
			if(value){
				$.ajax({
					url: $(this).data('href'),
					type: "POST",
					data: 'id_name=id&table_name=booking',
					success: function(data){
						tr.remove();
						table.draw();
						table_booking_silver_detail.draw();
						show_notify("Delete Sucessfully.",true);
					}
				});
			}
		});

	});


	function get_opneclose(){
		$.ajax({
			url: '<?=base_url('transaction/getOpenClose');?>',
			data: { as_on_data: $("#as_on_data").val()},
			method: "POST",
			success: function(result)
			{
				var obj = jQuery.parseJSON(result);
				var openData_gold = obj.openData.gold;
				if(openData_gold > 0 ){
					var openData_goldcolor = 'blue';
				}else if(openData_gold < 0){
					var openData_goldcolor = 'red';
				}

				var closeData_gold = obj.closeData.gold;
				if(closeData_gold > 0 ){
					var closeData_goldcolor = 'blue';
				}else if(closeData_gold < 0){
					var closeData_goldcolor = 'red';
				}
				//-------
				var openData_amount = obj.openData.amount;
				if(openData_amount > 0 ){
					var openData_amountcolor = 'blue';
				}else if(openData_amount < 0){
					var openData_amountcolor = 'red';
				}

				var closeData_amount = obj.closeData.amount;
				if(closeData_amount > 0 ){
					var closeData_amountcolor = 'blue';
				}else if(closeData_amount < 0){
					var closeData_amountcolor = 'red';
				}

				//---------
				var openData_silver = obj.openData.silver;
				if(openData_silver > 0 ){
					var openData_silvercolor = 'blue';
				}else if(openData_silver < 0){
					var openData_silvercolor = 'red';
				}

				var closeData_silver = obj.closeData.silver;
				if(closeData_silver > 0 ){
					var closeData_silvercolor = 'blue';
				}else if(closeData_silver < 0){
					var closeData_silvercolor = 'red';
				}


				$('#open_table').html('<tr><td>Gold</td><td><span style="color:'+openData_goldcolor+'">'+openData_gold+'</span></td></tr><tr><td>Amount</td><td><span style="color:'+openData_amountcolor+'">'+openData_amount+'</span></td></tr><tr><td>Silver</td><td><span style="color:'+openData_silvercolor+'">'+openData_silver+'</span></td></tr>');
				$('#close_table').html('<tr><td>Gold</td><td><span style="color:'+closeData_goldcolor+'">'+closeData_gold+'</span></td></tr><tr><td>Amount</td><td><span style="color:'+closeData_amountcolor+'">'+closeData_amount+'</span></td></tr><tr><td>Silver</td><td><span style="color:'+closeData_silvercolor+'">'+closeData_silver+'</span></td></tr>');
			}
		});
	}

	function get_ledger(id){
		$('#selected_account_id').val($(id).data('account-id'));
		$('#selected_account_name').val($(id).data('account-name'));
		$('#tab_ledger_tab').click();
		ledger_datatable.draw();
		return false;
	}
	function get_booking_detail(id){
		if(id != ''){
			$('#selected_booking_account_id').val($(id).data('account-id'));
			$('#selected_booking_account_name').val($(id).data('account-name'));
			$('#tab_booking_detail_tab').click();
			table_booking_gold_detail.draw();
			table_booking_silver_detail.draw();	
		}
		/*$('#selected_booking_account_id').val('');
		$('#selected_booking_account_name').val('');*/
		return false;
	}
	function account_name_load(){
		$.ajax({
			url: '<?=base_url('transaction/get_allaccount');?>',
			dataType: 'json',
			success: function(result){
				$("#account_name,#cut_account_name,#account_name1").select2({
					createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
					multiple: true,
					maximumSelectionSize: 1,
					data: result,
				});
				$("#account_name3").select2({
					createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
					multiple: true,
					maximumSelectionSize: 1,
					data: result,
				});
				$("#account_name_silver3").select2({
					createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
					multiple: true,
					maximumSelectionSize: 1,
					data: result,
				});
				$("#booking_account_name").select2({
					createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
					multiple: true,
					maximumSelectionSize: 1,
					data: result,
				});
				$("#booking_silver_account_name").select2({
					createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
					multiple: true,
					maximumSelectionSize: 1,
					data: result,
				});
				function log(e) {
					var e=$("<li>"+e+"</li>");
					$("#events_11").append(e);
					e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
				}
				$("#account_name,#cut_account_name")
					.on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
					.on("select2-opening", function() { log("opening"); })
					.on("select2-open", function() { log("open"); })
					.on("select2-close", function() { log("close"); })
					.on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
					.on("select2-focus", function(e) { log ("focus");})
					.on("select2-blur", function(e) { log ("blur");});

			}
		});
	}
	function via_load(){
		$.ajax({
			url: '<?=base_url('transaction/get_allvia');?>',
			dataType: 'json',
			success: function(result){
				$("#ddl_via").select2({
					createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
					multiple: true,
					maximumSelectionSize: 1,
					data: result,
				});
				function log(e) {
					var e=$("<li>"+e+"</li>");
					$("#events_11").append(e);
					e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
				}
				$("#ddl_via")
					.on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
					.on("select2-opening", function() { log("opening"); })
					.on("select2-open", function() { log("open"); })
					.on("select2-close", function() { log("close"); })
					.on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
					.on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
					.on("select2-focus", function(e) { log ("focus");})
					.on("select2-blur", function(e) { log ("blur");});

			}
		});
	}
</script>
<?php
$this->load->view('footer');
?>
