<?php 
$user_loginarry = $this->session->userdata("is_logged_in");
$login_admin_name = $user_loginarry['admin_lname'];
$login_admin_email = $user_loginarry['admin_email'];
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title> BDA | Dashboard</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?= base_url();?>assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?= base_url();?>assets/bootstrap/css/custom.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		<!-- jvectormap -->
		<link rel="stylesheet" href="<?= base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css');?>">


		<!----------------Notify---------------->
		<link rel="stylesheet" href="<?=base_url('assets/plugins/notify/jquery.growl.css');?>">
		<!----------------Notify---------------->



		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
		<!-- Select2 -->
		<link rel="stylesheet" href="<?= base_url();?>assets/plugins/select2/select2.css">
		<!-- bootstrap datepicker -->
		<link rel="stylesheet" href="<?= base_url();?>assets/plugins/datepicker/datepicker3.css">

		<!-- jQuery 2.2.3 -->
		<script src="<?= base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
		<!-- DataTables -->
		<link rel="stylesheet" href="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>" type="text/css">
		<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />

		<!-- Theme style -->
		<link rel="stylesheet" href="<?= base_url('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="<?= base_url('assets/dist/css/skins/_all-skins.min.css');?>">
		<link href="<?= base_url('assets/plugins/sweetalert/dist/sweetalert.css');?>" rel="stylesheet" type="text/css">
		<link href="<?= base_url('assets/dist/css/custom.css');?>" rel="stylesheet" type="text/css" />

		<?php
		if($this->uri->segment(1) == 'transaction'){
		?>

		<!-- daterange picker -->
		<link rel="stylesheet" href="<?= base_url()?>assets/plugins/daterangepicker/daterangepicker.css">
		<!-- bootstrap datepicker -->
		<link rel="stylesheet" href="<?= base_url()?>assets/plugins/datepicker/datepicker3.css">
		<!-- datatables -->
		<link rel="stylesheet" href="<?= base_url()?>assets/plugins/datatables/dataTables.bootstrap.css">
		<?php
		}
		?>
		<style>
			#container
			{
				position: relative;
			}
			input[name=""]
			{
				position: absolute;
				top: 0;
				left: 0;
				z-index: 999;
				padding: 0;
				margin: 0;
			}
			#account_name
			{
				position: absolute;
				top: 0;
				left: 0;
				padding: 0;
				margin: 0;
			}
		</style>
	</head>
	<body class="hold-transition skin-blue layout-top-nav">
		<div class="wrapper">
			<header class="main-header">
				<!-- Logo -->
				<!--<a href="<?= base_url()?>" class="logo">
					
					<span class="logo-mini"><b>BDA</b></span>
					
					<span class="logo-lg"><b>BDA</b></span>
				</a>-->
				<!-- Header Navbar: style can be found in header.less -->
				<nav class="navbar navbar-static-top">
					 <div class="container">
					<div class="navbar-header">
						<a href="<?= base_url('transaction')?>" class="navbar-brand"><b>BDA</b></a>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>
					<!-- Sidebar toggle button-->
					<!--<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>-->
					<!-- Navbar Right Menu -->
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- User Account: style can be found in dropdown.less -->
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo base_url('assets/dist/img/user2-160x160.jpg');?>" class="user-image" alt="User Image">
									<span class="hidden-xs"><?php echo isset($login_admin_name)?ucwords($login_admin_name):'Admin';?></span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
										<img src="<?php echo base_url('assets/dist/img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
										<p>
											<?php echo isset($login_admin_name)?ucwords($login_admin_name):'Admin';?>
											<br/>
											<?php echo isset($login_admin_email)?$login_admin_email:'';?>
										</p>

									</li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
											<a href="<?= base_url()?>auth/profile" class="btn btn-default btn-flat">Change Password</a>
										</div>
										<div class="pull-right">
											<a href="<?= base_url()?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
										</div>
									</li>
								</ul>
							</li>
							<!-- Control Sidebar Toggle Button -->
						</ul>
					</div>
					</div>
				</nav>
			</header>