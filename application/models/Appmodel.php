<?php
ini_set('max_execution_time', 3000);

/**
 * Class AppModel
 * @property CI_DB_active_record $db
 */
class AppModel extends CI_Model
{
    protected $currCompetition = 0;
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * ------------------- Required Methods
     */

    /**
     * @param $username
     * @param $pass
     * @return bool
     */
    function login($username,$pass){
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('admin_email',$username);
        $this->db->where('admin_password',md5($pass));
        $this->db->limit('admin');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
    }
	
	function delete($table_name,$where_array){
		$result = $this->db->delete($table_name,$where_array);
		return $result;
	}

    
    public function check_party_email_validation($email, $id)
    {
        $duplicate = 0;
        if($id > 0)
        {
            $email = trim(strtolower($email));
            $sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(email_id)) LIKE '%".addslashes($email)."%' AND party_id != $id";
            $query = $this->db->query($sql_list_query);
            $rows = $query->result_array();
            if(count($rows) > 0)
            {
                $duplicate = 1;
            }                  
        }   
        else
        {
            $email = trim(strtolower($email));
            $sql_list_query = "SELECT * FROM party WHERE TRIM(LOWER(email_id)) LIKE '%".addslashes($email)."%'";
            $query = $this->db->query($sql_list_query);
            $rows = $query->result_array();
            if(count($rows) > 0)
            {
                $duplicate = 1;
            }                  
        } 

        return $duplicate;     
    }

    /**
     * ------------------- Required Methods
     */

    /**
     * @param $table
     * @param $column_order
     * @param $column_search
     * @param int $user_type
     */

    private function get_datatables_query($table,$column_order,$column_search,$user_type = 3)
    {
        $this->db->where('competition_id =',$this->currCompetition);
        $this->db->where('user_type !=',1);
        if($user_type == 2){
            $this->db->where('user_type !=',3);
        }else{
            $this->db->where('user_type !=',2);
        }
        $this->db->from($table);
        $i = 0;
        foreach ($column_search as $item) // loop column
        {
            if(isset($_POST['search']) && $_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($column_order))
        {
            $order = $column_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

    }
    function get_datatables($table,$column_order,$column_search,$user_type = 3)
    {
        $this->get_datatables_query($table,$column_order,$column_search,$user_type);
        if(isset($_POST['length']) && $_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($table,$column_order,$column_search,$user_type = 3)
    {
        $this->get_datatables_query($table,$column_order,$column_search,$user_type);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($table,$user_type = 3)
    {
        $this->db->from($table);
        $this->db->where('user_type',$user_type);
        $this->db->where('competition_id',$this->currCompetition);
        return $this->db->count_all_results();
    }

    /**
     * @param $table
     * @param $column
     * @param $search_value
     * @return mixed
     */
    function getAutoCompleteData($table,$column,$search_value){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->like($column,$search_value);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * @param $table
     * @param $id_column
     * @param $column
     * @param $column_val
     * @return int
     */
    function get_id_by_val($table,$id_column,$column,$column_val){
        $this->db->select($id_column);
        $this->db->from($table);
        $this->db->where($column,$column_val);
        $this->db->limit('1');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row()->$id_column;
        } else {
        	return null;
        }
        
        /* else{
            $this->db->insert($table,array($column=>$column_val));
            return $this->db->insert_id();
        } */
    }


    /**
     * @param $table_name
     * @param $column_nm
     * @param $column_val
     * @return bool
     */
    function check_val_exist($table_name,$column_nm,$column_val){
        $this->db->where($column_nm,$column_val);
        $query = $this->db->get($table_name);
        return $query->num_rows() > 0;
    }

    /**
     * @return mixed
     */
    public function get_config()
    {
        return $this->db->get('config');
    }
    
    function get_allMasterRecored($table,$id,$name){
        $query = $this->db->query("SELECT `" . $id . "`  as id,`" . $name . "` as text FROM `" . $table . "` ORDER BY `" . $name . "` ASC");
        return $query->result();
    }
    
    function get_result_where($tbl_name,$where,$where_id)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where,$where_id);
		$query = $this->db->get();
		return $query->result();
	}

    /**
     * Generic model for select data from any table where multiple params
     * can be pass.
     * 
     * @params $table pass the name of table
     * @params $fields = array() pass the array of fields whic you want to select
     * @params $conditions = array() 
     * @params $sort = array() 
     * @params $group = array() 
     * 
     * @return array mysql result
     * 
     */
    function getData($table, $fields = array(), $conditions = array(), $sort = array(), $group = array())
	{
		$fields = implode(",", $fields);

		$this->db->select("$fields");

		$this->db->from($table);
		
		foreach ($conditions as $key => $val) {
			$condtion = $val['condtion'];
			$this->db->$condtion($val['column'],$val['value']);
		}

		foreach ($sort as $key => $val) {
			$this->db->order_by($key, $val);
		}

		foreach ($group as $key => $val) {
			$this->db->group_by($val); 
		}

		$query = $this->db->get();

		// echo $this->db->last_query();

		return $query->result();
	}

    function insert($table_name,$data_array)
    {
		if($this->db->insert($table_name,$data_array)) {
			return $this->db->insert_id();
		}
		return false;
	}

    function booking_summary_gold(){
		$this->db->select('b.*, ac.account_name, SUM(b.weight) as gold_weight, SUM(b.rate * b.weight) AS t1');
		$this->db->from('booking b');
		$this->db->join('account ac','ac.account_id = b.account', 'left');
		$this->db->where('b.type',1);
		 $this->db->group_by('b.account'); 
		$query = $this->db->get();
		return $query->result_array();
	}
	function booking_summary_silver(){
		$this->db->select('b.*, ac.account_name, SUM(b.weight) as silver_weight, SUM(b.rate * b.weight) AS t1');
		$this->db->from('booking b');
		$this->db->join('account ac','ac.account_id = b.account', 'left');
		$this->db->where('b.type',2);
		 $this->db->group_by('b.account'); 
		$query = $this->db->get();
		return $query->result_array();
	}
	function booking_gold_summary(){
		$this->db->select('b.*, SUM(b.weight) as gold_weight, SUM(b.rate * b.weight) AS g1');
		$this->db->from('booking b');
		$this->db->where('b.type',1);
		$query = $this->db->get();
		return $query->result_array();
	}
	function booking_silver_summary(){
		$this->db->select('b.*, SUM(b.weight) as silver_weight, SUM(b.rate * b.weight) AS s1');
		$this->db->from('booking b');
		$this->db->where('b.type',2);
		$query = $this->db->get();
		return $query->result_array();
	}
	function open_summary(){
		$this->db->select('SUM(gold) as open_gold,SUM(silver) as open_silver,SUM(amount) as open_amount');
		$this->db->from('transaction_record');
		$this->db->where('rs_effect_balance',1);
		$this->db->where('gold_effect_balance',1);
		$this->db->where('silver_effect_balance',1);
		$this->db->where('date < NOW()');
		$query = $this->db->get();
		return $query->result_array();
	}

	function close_summary(){
		$this->db->select('SUM(gold) as close_gold,SUM(silver) as close_silver,SUM(amount) as close_amount');
		$this->db->from('transaction_record');
		$this->db->where('rs_effect_balance',1);
		$this->db->where('gold_effect_balance',1);
		$this->db->where('silver_effect_balance',1);
		$this->db->where('date <= NOW()');
		$query = $this->db->get();
		return $query->result_array();
	}
	function account_array(){
		$this->db->select('account_id, account_name');
		$this->db->from('account');
		$this->db->order_by('last_updated', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}
	function bs_g_summary($id){
		$this->db->select('b.*, SUM(b.weight) as gold_weight, SUM(b.rate * b.weight) AS t1');
		$this->db->from('booking b');
		$this->db->where('b.type',1);
		$this->db->where('b.account',$id);
		$this->db->where('b.rate != ',0,FALSE);
		$query = $this->db->get();
		return $query->result_array();
	}
	function bs_s_summary($id){
		$this->db->select('b.*, SUM(b.weight) as silver_weight, SUM(b.rate * b.weight) AS t1');
		$this->db->from('booking b');
		$this->db->where('b.type',2);
		$this->db->where('b.account',$id);
		$this->db->where('b.rate != ',0,FALSE);
		$query = $this->db->get();
		return $query->result_array();
	}
	function transaction_summary($id){
		$this->db->select(' SUM(t.amount) as total_amount, SUM(t.gold) as total_gold, SUM(t.silver) as total_silver');
		$this->db->from('transaction_record t');
		$this->db->join('via_master vm','vm.id = t.via_id', 'left');
		$this->db->where('t.account',$id);
		$query = $this->db->get();
		return $query->result_array();		
	}
}
