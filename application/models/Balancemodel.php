<?php
ini_set('max_execution_time', 3000);

/**
 * Class AppModel
 * @property CI_DB_active_record $db
 */
class BalanceModel extends CI_Model
{
    protected $currCompetition = 0;
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * ------------------- Required Methods
     */

    /**
     * @param $username
     * @param $pass
     * @return bool
     */
     function getAccountTransaction($userId){	
		$sql_list_query = "SELECT * FROM transaction_record WHERE account = $userId";
		$query = $this->db->query($sql_list_query);
		$rows = $query->result_array();
		//print_r($rows);
		if(count($rows) > 0)
		{
			$duplicate = 1;
			return $rows;
		}
		return false;
    }

    function getGoldBookings($userId){
		//$email = trim(strtolower($email));
		$sql_list_query = "SELECT * FROM `booking` WHERE `type` = 1 AND `account` = $userId";
		$query = $this->db->query($sql_list_query);
		$rows = $query->result_array();
		//print_r($rows);
		$grand_booking_weight=0;
		if(count($rows) > 0)
		{
			foreach($rows as $single_row)
			{
				$grand_booking_weight=$grand_booking_weight+$single_row['weight'];
			}
			$gold_bookings['rows']=$rows;
			$gold_bookings['grand_booking_weight']=$grand_booking_weight;
			$gold_bookings['total_records']=count($rows);
			return $gold_bookings;
		}
		return 0;
    }

    function getSilverBookings($userId){
		//$email = trim(strtolower($email));
		$sql_list_query = "SELECT * FROM `booking` WHERE `type` = 2 AND `account` = $userId";
		$query = $this->db->query($sql_list_query);
		$rows = $query->result_array();
		//print_r($rows);
		if(count($rows) > 0)
		{
			$duplicate = 1;
			return $rows;
		}
		return 0;
    }

    function updateGoldBookings($userId,$weight,$transactionId){
		//$email = trim(strtolower($email));
		$sql_list_query = "SELECT * FROM `booking` WHERE `type` = 1 AND `account` = $userId";
		$query = $this->db->query($sql_list_query);
		$rows = $query->result_array();
		//print_r($rows);
		if(count($rows) > 0)
		{
			$duplicate = 1;
			return $rows;
		}
		return false;
    }
}
