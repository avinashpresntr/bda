ALTER TABLE `transaction_record` ADD `pending_gold` DOUBLE NULL DEFAULT NULL AFTER `gold`;
ALTER TABLE `booking` ADD `pending_weight` DOUBLE NULL DEFAULT NULL AFTER `weight`;

CREATE TABLE `trans_book_log` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `trans_id` INT(11) NOT NULL ,  `book_id` INT(11) NOT NULL ,  `used_qty` DOUBLE NOT NULL ,  `notes` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,  `created_by` INT(11) NOT NULL ,  `created_at` DATETIME NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_unicode_ci;

ALTER TABLE `transaction_record` ADD `pending_silver` DOUBLE NULL AFTER `silver`;

