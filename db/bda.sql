-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 28, 2017 at 08:48 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bda`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `account_id` int(11) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `last_updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`account_id`, `account_name`, `last_updated`, `created_at`) VALUES
(1, 'User 1', '2017-03-03 10:09:35', '2017-02-17 16:14:15'),
(2, 'User 2', '2017-03-03 10:51:38', '2017-02-17 17:14:15'),
(3, 'User 3', '2017-02-20 16:05:22', '2017-02-17 17:14:15'),
(4, 'User 4', '2017-02-20 21:38:01', '2017-02-20 18:31:55'),
(5, 'User 5', '2017-02-20 21:37:42', '2017-02-20 19:02:37'),
(6, 'User 6', '2017-02-20 16:05:22', '2017-02-20 19:24:12'),
(7, 'Vipul', '2017-03-03 10:53:05', '2017-02-20 22:05:42'),
(8, 'trilok', '2017-03-28 11:22:17', '2017-02-20 22:08:59'),
(9, 'ashok', '2017-03-06 11:47:25', '2017-02-20 22:09:23');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(22) NOT NULL,
  `admin_lname` varchar(22) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_password` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_email`, `admin_img`, `admin_password`) VALUES
(1, 'main', 'admin', 'admin@gmail.com', '', '75d23af433e0cea4c0e45a56dba18b30');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1 = gold 2 = silver',
  `booking_date` date NOT NULL,
  `account` int(11) NOT NULL,
  `rate` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `mcx` double DEFAULT NULL,
  `booking_by` int(11) NOT NULL COMMENT '1 = cash  2 = bill'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `type`, `booking_date`, `account`, `rate`, `weight`, `mcx`, `booking_by`) VALUES
(1, 1, '2017-03-27', 9, 100, 100, 100, 1),
(2, 1, '2017-03-27', 9, 200, 200, 200, 2),
(3, 1, '2017-03-28', 1, 200, 111, 212, 1),
(4, 1, '2017-03-28', 1, 444, 200, 333, 1),
(5, 2, '2017-03-28', 2, 1000, 100, 777, 1),
(6, 2, '2017-03-28', 2, 3000, 100, 50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_record`
--

CREATE TABLE `transaction_record` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `account` int(11) NOT NULL,
  `gold` double NOT NULL,
  `silver` double NOT NULL,
  `amount` double NOT NULL,
  `rs_effect_balance` int(11) NOT NULL DEFAULT '0',
  `gold_effect_balance` int(11) NOT NULL DEFAULT '0',
  `silver_effect_balance` int(11) NOT NULL DEFAULT '0',
  `via_id` int(11) NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_record`
--

INSERT INTO `transaction_record` (`id`, `date`, `account`, `gold`, `silver`, `amount`, `rs_effect_balance`, `gold_effect_balance`, `silver_effect_balance`, `via_id`, `notes`, `created_at`, `updated_at`) VALUES
(1, '2017-02-16', 1, 400, 300, 300, 0, 0, 0, 4, NULL, '2017-02-17 17:17:06', '2017-02-17 11:47:20'),
(2, '2017-02-17', 2, 0, 0, 300, 0, 0, 0, 0, NULL, '2017-02-17 17:17:30', '2017-02-17 11:47:30'),
(3, '2017-02-17', 1, -100, -50, -200, 0, 0, 0, 5, NULL, '2017-02-17 17:17:57', '2017-02-17 11:47:57'),
(4, '2017-02-20', 1, 50, 25, 50, 0, 0, 0, 4, NULL, '2017-02-20 16:21:15', '2017-02-20 10:51:15'),
(5, '2017-02-20', 2, 200, 100, -100, 0, 0, 0, 5, NULL, '2017-02-20 17:06:15', '2017-02-20 11:36:15'),
(6, '2017-02-20', 3, 100, 100, 100, 0, 0, 0, 1, NULL, '2017-02-20 17:06:46', '2017-02-20 11:36:46'),
(7, '2017-02-20', 1, 100, 100, 100, 0, 0, 0, 4, NULL, '2017-02-20 17:34:48', '2017-02-20 12:04:48'),
(8, '2017-02-20', 4, -200, 100, 500, 0, 0, 0, 3, NULL, '2017-02-20 18:31:55', '2017-02-20 13:01:55'),
(9, '2017-02-20', 2, 100, 50, 200, 0, 0, 0, 2, NULL, '2017-02-20 19:02:12', '2017-02-20 13:32:12'),
(10, '2017-02-20', 5, 100, 0, 100, 1, 1, 1, 1, NULL, '2017-02-20 19:02:37', '2017-03-03 05:37:41'),
(12, '2017-02-20', 6, 100, 0, 200, 1, 1, 1, 4, NULL, '2017-02-20 19:24:12', '2017-03-03 05:37:52'),
(14, '2017-02-20', 4, 11, 11, 11, 0, 0, 0, 0, NULL, '2017-02-20 21:38:01', '2017-02-20 16:08:01'),
(16, '2017-02-20', 7, 2, 3, 12, 1, 1, 1, 1, NULL, '2017-02-20 22:06:20', '2017-03-03 05:38:04'),
(21, '2017-03-03', 1, 16, 5, -10, 1, 1, 1, 7, 'test', '2017-03-03 09:06:54', '2017-03-06 05:55:35'),
(26, '2017-03-23', 7, 18, 27, 6, 1, 1, 1, 2, 'test all data', '2017-03-03 10:27:43', '2017-03-03 04:57:43'),
(27, '2017-03-23', 2, 65, 85, 45, 1, 1, 1, 5, 'full t', '2017-03-03 10:51:38', '2017-03-03 05:21:38'),
(28, '2017-03-08', 7, 108, 154, 78, 0, 0, 0, 5, 'test open', '2017-03-03 10:53:05', '2017-03-03 05:23:05'),
(29, '2017-03-06', 8, 15, 50, 1000, 1, 1, 1, 4, '', '2017-03-06 11:36:50', '2017-03-06 06:06:50'),
(30, '2017-03-06', 9, 50, 100, 500, 0, 0, 0, 5, '', '2017-03-06 11:38:10', '2017-03-06 06:10:53'),
(31, '2017-03-06', 8, 10, 0, -10, 0, 0, 0, 7, '', '2017-03-06 11:46:04', '2017-03-06 06:16:04'),
(32, '2017-03-06', 9, -15, 0, 15, 0, 0, 0, 7, '', '2017-03-06 11:47:25', '2017-03-06 06:17:25'),
(33, '2017-03-27', 8, 1, 1, 100, 0, 0, 0, 2, 'test', '2017-03-27 14:45:22', '2017-03-27 09:15:22'),
(34, '2017-03-28', 8, 100, 100, 100, 0, 0, 0, 0, '100', '2017-03-28 11:22:17', '2017-03-28 05:52:17');

-- --------------------------------------------------------

--
-- Table structure for table `via_master`
--

CREATE TABLE `via_master` (
  `id` int(11) NOT NULL,
  `via` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `via_master`
--

INSERT INTO `via_master` (`id`, `via`, `updated_at`) VALUES
(1, 'Cash', '2017-02-15 15:06:29'),
(2, 'BOB', '2017-02-15 15:06:29'),
(3, 'HDFC', '2017-02-15 15:06:29'),
(4, 'Axis', '2017-02-15 15:06:29'),
(5, 'Card', '2017-02-15 15:06:29'),
(6, 'None', '2017-02-15 15:06:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_record`
--
ALTER TABLE `transaction_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `via_master`
--
ALTER TABLE `via_master`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `transaction_record`
--
ALTER TABLE `transaction_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `via_master`
--
ALTER TABLE `via_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
